class Contact < ActiveRecord::Base
	belongs_to :user
	
	validates :phone_number, :details, presence: true
end
