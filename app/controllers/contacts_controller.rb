class ContactsController < ApplicationController
	before_filter :authenticate_user!, only: :index
	
	def index
		@user = User.find(params[:id])
		@contacts = @user.contacts.order(updated_at: :desc)
		
		#binding.pry
		
		authorize! :show, @user
		respond_to do |format|
			format.js
			format.html
		end
	end
	
	def show
		#code
	end
	
	def new
		@contact = Contact.new
		#@user = User.find(params[:user_id])
		#@contact_params = Contact.where(user_id: params[:user_id])
	end
	
	def edit
    @contact = Contact.find(params[:id])
    #@user = User.joins(:contacts).where("contacts.id = #{params[:id]}")
    respond_to do |format|
			format.js
		end
    #binding.pry
    #authorize! :manage, @contact
  end
	
	def create
		#byebug
		@contact = Contact.new(contact_params)
		@user = User.find(@contact.user_id)
		@contacts = @user.contacts
		saved = @contact.save
		
		respond_to do |format|
			format.js
			format.html do
				if saved
					redirect_to contact_path(@contact.user_id)
				else 
					render :new
				end
			end
		end
	end

	def update
		#binding.pry
		@contact = Contact.find(params[:id])
    @user = User.find(params[:user_id])
    @contacts = @user.contacts
    updated = @contact.update(contact_params)
    
    respond_to do |format|
			format.js
			format.html do
				if @contact.errors.blank?
					if updated
						#flash[:notice] = "Data has been updated"
						redirect_to contact_path(@contact.user_id)
					end
				else
					render :edit
					#flash[:notice] = "Contact has been successfully updated."
				end
			end
		end
	end
	
	def destroy
		@contact = Contact.find(params[:id])
		@user = User.find(params[:user_id])
		@contact.destroy
		flash[:notice] = "Data has been removed"
		#redirect_to :back
		redirect_to contact_path(@user.id), notice: "Contact has been successfully destroy."
	end
	
	private
  def contact_params
    params.require(:contact).permit(:details, :phone_number, :user_id)
  end
	
end
