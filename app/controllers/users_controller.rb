class UsersController < ApplicationController
    #before_filter :authenticate_user!, :except => [:show, :new]
  	before_filter :authenticate_user!, :only => :index

  def index
    #@user = User.all
    #@users = User.order(id: :asc)
    @count = Contact.count 
    @users = User.find_by_sql(
      "select t1.*, count(distinct t2.id) as total
      from users t1
      left join contacts t2 on t1.id = t2.user_id
      group by t1.id
      order by t1.id"
    )
    if cannot? :manage, @users
      redirect_to contact_path(current_user.id)
    end
  end
   
  def show
    @user = User.find(params[:id])
    authorize! :show, @user
    #if can? :read, @user
    #   unless @user == current_user
    #    redirect_to users_path, :alert => "Access denied."
    #  end
    #end
    
  end
  
  def new
    @user = User.new
  end
  
  def edit
    @user = User.find(params[:id])
    #binding.pry
    authorize! :manage, @user
  end
  
  def create
    @user = User.new(user_params)
    authorize! :manage, @user
    if @user.save
      redirect_to users_path
    else
      render 'new'
    end
  end
  
  def update
    @user = User.find(params[:id])
    
    if params[:user][:password].blank?
      @user.update_without_password(user_params)
    else
      @user.update(user_params)
    end
    
    if @user.errors.blank?
      redirect_to users_path, :notice => "User information updated successfully."
    else
      render :edit
    end
    
    #if @user.update(user_params)
    #  redirect_to users_path
    #else
    #  render 'edit'
    #  #redirect_to edit_user_path
    #  #flash[:alert] = 'error'
    #end
  end
  
  def destroy
    @user = User.find(params[:id])
    @user.destroy
    flash[:notice] = "Data have been removed"
    redirect_to :back
  end
  
  #binding.pry
  private
  def user_params
    params.require(:user).permit(:name, :access_group, :email, :password, :password_confirmation)
  end
  
  

end
