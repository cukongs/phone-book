class CreateContacts < ActiveRecord::Migration
  def change
    create_table :contacts do |t|
      t.references :user, index:true
      t.string  :phone_number
      t.string  :details
      t.timestamps null: false
    end
  end
end
