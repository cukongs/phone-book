Rails.application.routes.draw do
  root to: 'users#index'
  devise_for :users
  
  post '/users/new', to: 'users#create'
  get '/contacts/:id', to: 'contacts#index'
  get '/contacts/:user_id/new', to: 'contacts#new'
  post '/contacts/:id', to: 'contacts#create'
  delete '/contacts/:id', to: 'contacts#destroy'
  patch '/contacts/:id/:user_id', to: 'contacts#update'
  resources :users, :contacts

end
