include Warden::Test::Helpers
Warden.test_mode!
# Feature: User edit
#   As a user
#   I want to edit my user profile
#   So I can change my email address

feature 'Contact create', :devise do
  
  before :each do
    @user = FactoryGirl.create(:user)
		login_as(@user)
	end
  
  #after(:each) do
  #  Warden.test_reset!
  #end

  # Scenario: User changes email address
  #   Given I am signed in
  #   When I change my email address
  #   Then I see an account updated message
  scenario 'user create contact', js: true do
    visit contact_path(@user.id)
    fill_in 'Phone number', with: '12345678'
    sleep 2
    fill_in 'Details', with: 'tester'
    sleep 2
    click_button 'Add'
    sleep 1
    click_on 'delete'
    #page.find('.delete_action').click
		sleep 2
  end

end
