include Warden::Test::Helpers
Warden.test_mode!
# Feature: User edit
#   As a user
#   I want to edit my user profile
#   So I can change my email address

feature 'Contact delete', :devise do
  
  before :each do
    @user = FactoryGirl.create(:user)
    @contact = FactoryGirl.create(:contact)
		login_as(@user)
	end
  
  after(:each) do
    Warden.test_reset!
  end

  # Scenario: User changes email address
  #   Given I am signed in
  #   When I change my email address
  #   Then I see an account updated message
  scenario 'user delete contact', js: true do
    visit contact_path(@user.id)
    click_on 'delete'
    sleep 2
    #page.driver.browser.switch_to.alert.accept
  end

end
