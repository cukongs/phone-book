# Feature: Sign in
#   As a user
#   I want to sign in
#   So I can visit protected areas of the site
require "rails_helper"
feature 'Sign in', :devise do

  # Scenario: User cannot sign in if not registered
  #   Given I do not exist as a user
  #   When I sign in with valid credentials
  #   Then I see an invalid credentials message
  #scenario 'user cannot sign in if not registered' do
  #  signin('cukongonly@gmail.com', 'tanggallahir')
  #  expect(page).to have_content I18n.t 'devise.failure.not_found_in_database', authentication_keys: 'email'
  #end

  # Scenario: User can sign in with valid credentials
  #   Given I exist as a user
  #   And I am not signed in
  #   When I sign in with valid credentials
  #   Then I see a success message
  #scenario 'user can sign in with valid credentials' do
  #  user = FactoryGirl.create(:user)
  #  signin(user.email, user.password)
  #  expect(page).to have_content I18n.t 'devise.sessions.signed_in'
  #end

  # Scenario: User cannot sign in with wrong email
  #   Given I exist as a user
  #   And I am not signed in
  #   When I sign in with a wrong email
  #   Then I see an invalid email message
  #scenario 'user cannot sign in with wrong email' do
  #  user = FactoryGirl.create(:user)
  #  signin('invalid@email.com', user.password)
  #  expect(page).to have_content I18n.t 'devise.failure.not_found_in_database', authentication_keys: 'email'
  #end

  # Scenario: User cannot sign in with wrong password
  #   Given I exist as a user
  #   And I am not signed in
  #   When I sign in with a wrong password
  #   Then I see an invalid password message
  #scenario 'user cannot sign in with wrong password' do
  #  user = FactoryGirl.create(:user)
  #  signin(user.email, 'invalidpass')
  #  expect(page).to have_content I18n.t 'devise.failure.invalid', authentication_keys: 'email'
  #end
  before :each do
    @user = FactoryGirl.create(:user)
  end
  
  scenario 'with valid credentials', js: true do
    sign_in_user('cukongonly@gmail.com', 'tanggallahir')
    #visit new_user_session_path
    #fill_in 'user_email', with: 'cukongonly@gmail.com'
    #sleep 2
    #fill_in 'user_password', with: 'tanggallahir'
    #sleep 2
    #click_button 'Sign in'
    ##expect(page).to have_content('You have successfully signed in!')
  end
  
  scenario 'with invalid credentials', js: true do
    sign_in_user('aaa@aaa.aaa.com', 'tanggallahir')
  end
end