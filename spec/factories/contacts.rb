FactoryGirl.define do
  factory :contact do
    user_id 1
    phone_number 123456
    details "tester"
  end
end
