describe UsersController do
  
  let(:valid_attributes) {
    {
      name: 'Cukong',
      email: 'cukongonly@gmail.com',
      password: 'cukong123456'
    }
  }
  
  let(:valid_session) {
    {
      "title" => "tester"
    }
  }
  
  let(:nil_atrributes) {
    {
      "name" => nil,
      "email" => nil,
      "password" => nil
    }
  }
  
  let(:new_attributes) {
    #skip("Add a hash of attributes valid for your model")
    {
      name: 'Rizal'
    }
  }
  
  #describe "GET 'index'" do
  #  before { get :index }
  #  it { should respond_with(302) }
  #end

  
  describe "when signed in" do
    
    #describe "GET 'new'" do
    #  it "assigns a new sign up user" do
    #    get :new, {}, valid_session
    #    expect(assigns(:user)).to be_a_new(User)
    #  end
    #end
    
    it "blocks unauthenticated access redirect to sign up" do
      sign_in :nil_atrributes
      get :index
      should redirect_to(new_user_session_path)
    end
    
    it "allows authenticated access" do
      sign_in :valid_atrributes
      get :index
      #should respond_with(302)
    end
    
  end      
  
  describe "after sign in" do
    
    #describe 'GET :index' do
      #it "blocks unauthenticated access" do
        #sign_in(nil)
        #get :index
        #response.should redirect_to(new_user_registration_path)
      #end  
    #  it "allows authenticated acces" do
    #    get :index
    #    expect(response).to be_redirect
    #  end
    #end
    before(:each) do
      request.env["HTTP_REFERER"] = "_back"
    end
    describe "GET show" do
      it "assigns the requested user information" do
        user = User.create! valid_attributes
        get :show, {:id => user.to_param}, valid_session
        expect(assigns(:user)).to eq(user)
        should respond_with(302)
      end
    end
    
    describe "POST create" do
      describe "with valid params" do
        it "creates a new User" do
          post :create, user: valid_attributes #or FactoryGirl.attributes_for(:user)
          get :index
          should respond_with(302)
          #should redirect_to(users)
          #expect { User.create(:user => valid_atrributes) }.to change{ User.count }.by(1)
        end
      end
    end
    
    describe "GET edit" do
      it "assigns the requested user" do
        user = User.create! valid_attributes
        get :edit, {id: user.to_param}
        expect(assigns(:user)).to eq(user)
      end
    end
    
    describe "GET update" do
      it "assigns the requested user" do
        user = User.create! valid_attributes
        patch :update, {:id => user.to_param, :user => new_attributes}
        expect(assigns(:user)).to eq(user)
        should redirect_to(users_path)
      end
    end
    
    describe "DELETE destroy" do
      it "deletes the requested contact" do
        user = User.create! valid_attributes
        delete :destroy, {:id => user.to_param}
        should redirect_to("_back")
        #should respond_with(302)
        #expect{
        #  delete :destroy,
        #  user:FactoryGirl.attributes_for(:user)
        #}.to change(User, :count).by(-1)
      end
    end
  
  end
  
  
  
end