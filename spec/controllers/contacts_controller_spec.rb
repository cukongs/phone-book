require "rails_helper"

describe ContactsController do
	before :each do
		@user = FactoryGirl.create(:user)
		@contact = FactoryGirl.create(:contact)
		sign_in(@user)
	end
	
	let(:valid_attributes) {
    {
      phone_number: 12345678,
      details: 'home',
      user_id: 1
    }
  }
	
	let(:login_att){
		{
			email: 'cukongonly@gmail.com',
			password: 'asasdasdsdasd'
		}
	}
	
	describe "after sign in" do
		
    #before(:each) do
    #  request.env["HTTP_REFERER"] = "_back"
    #end
    
    #describe "GET show" do
    #  it "assigns the requested contact information" do
    #    contact = Contact.create! valid_attributes
    #    get :show, {:id => contact.to_param}
    #    expect(assigns(:contact)).to eq(contact)
    #    #should respond_with(302)
    #  end
    #end
    
    describe "POST create" do
			
      describe "with valid params" do
        it "creates a new Contact" do
					contact = Contact.create! valid_attributes
          post :create, {contact: valid_attributes, user_id: contact.user_id}
					should redirect_to(contact_path(contact.user_id))
        end
      end
    end
    
    describe "GET edit" do
      it "assigns the requested contact as @contact" do
        contact = Contact.create! valid_attributes
        xhr :get, :edit, {id: contact.to_param}
        expect(assigns(:contact)).to eq(contact)
      end
    end
    
    describe "GET update" do
      it "assigns the requested contact as @contact" do
        contact = Contact.create! valid_attributes
        patch :update, {contact: valid_attributes, user_id: @user.id, id: contact.to_param}
        expect(assigns(:contact)).to eq(contact)
        should redirect_to contact_path(@contact.user_id)
      end
    end
    
    describe "DELETE destroy" do
      it "deletes the requested contact" do
        contact = Contact.create! valid_attributes
        delete :destroy, {id: contact.to_param, user_id: @user.id}
        should redirect_to contact_path(@user.id)
      end
    end
  end
	
end