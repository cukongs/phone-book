require "rails_helper"

RSpec.describe Contact do
  
  before(:each) do
    @contact = Contact.new(phone_number: 123456, details: "home" )
    #@user.email = "user@example.com"
  end
  
  it "has relation model to user" do
    expect belong_to(:user) 
  end
  
  #it { should respond_to(:phone_number) }
  
  #it "should have valid factory" do
  #  FactoryGirl.build(:detail).should be_valid
  #end
  
  it "should require a #phone_number,#email" do
    FactoryGirl.build(:contact, {phone_number: nil, details: nil} ).should_not be_valid
  end
  
  it "should be a valid #phone_number,#email" do
    FactoryGirl.build(:contact, {
      phone_number: @contact.phone_number,
      details: @contact.details } ).should be_valid
  end
  
end
