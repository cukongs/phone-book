require "rails_helper"

#describe User do
RSpec.describe User do

  #before(:each) {
  #  @user = User.new(email: 'user@example.com')
  #}
  
  before(:each) do
    @user = User.new(email: 'user@example.com')
    #@user.email = "user@example.com"
  end
  
  it "should have many contact" do
    t = User.reflect_on_association(:contacts)
    t.macro.should == :has_many
  end
  
  #subject { @user }
  
  it { should respond_to(:email) }
  
  it "should have valid factory" do
    FactoryGirl.build(:user).should be_valid
  end
  
  it "should require a #email" do
    FactoryGirl.build(:user, :email => "").should_not be_valid
  end
  
  it "should be valid #email" do
    FactoryGirl.build(:user, :email => @user.email).should be_valid
  end
  
  it "#email match and returns string or response" do
    expect(@user.email).to match 'user@example.com'
  end
  
  #describe "email" do
    # using the accept_values_for gem
    #it { should_not accept_values_for(:name, nil) } 
    #it { should_not accept_values_for(:name, "a") } 
    #it { should_not accept_values_for(:name, "a" * 201) } 
    #it { should accept_values_for(:name, "a" * 2) }
    #it { should accept_values_for(:name, "a" * 200) }
    
  #end

end
