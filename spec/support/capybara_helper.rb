module CapybaraHelper
  def sign_up_user(name, email, password, password_confirmation)
    visit new_user_registration_path
    fill_in 'Name', with: name
    sleep 2
    fill_in 'Email', with: email
    sleep 2
    fill_in 'Password', with: password
    sleep 2
    fill_in 'Password confirmation', :with => password_confirmation
    sleep 2
    click_button 'Sign up'
    sleep 2
  end

  def sign_in_user(email, password)
    visit new_user_session_path
    fill_in 'Email', with: email
    sleep 2
    fill_in 'Password', with: password
    sleep 2
    click_button 'Sign in'
    sleep 2
  end
  
end

